﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField] float speed = 10.0f;
    [SerializeField] float xMin=0, xMax=1, yMin=0, yMax=1;
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float minimumTimeBetweenShots = 0.5f;
    [SerializeField] float laserVelocity = 5.0f;
    Coroutine fireCoroutine;
    bool firing = false;
    void Start() {
        SetUpMoveBoundaries();
    }

    private void SetUpMoveBoundaries()
    {
        var camera = Camera.main;
        xMin = camera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + transform.lossyScale.x/2;
        xMax = camera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - transform.lossyScale.x/2;
        yMin = camera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + transform.lossyScale.y/2;
        yMax = camera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - transform.lossyScale.y/2;
    }

    void Update() {
        Move();
        CheckForFire();
    }

    private void CheckForFire()
    {
        if (Input.GetButtonDown("Fire1") && !firing)
        {
            fireCoroutine = StartCoroutine(FireContinuously());
            firing = true;
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(fireCoroutine);
            firing = false;
        }
    }

    IEnumerator FireContinuously()
    {
        while (true)
        {
            Fire();
            yield return new WaitForSeconds(minimumTimeBetweenShots);
        }
    }

    private void Fire()
    {
        var laser = Instantiate(laserPrefab, transform.transform.position, transform.rotation) as GameObject;
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, laserVelocity);
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime *speed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        var newX = Mathf.Clamp(transform.position.x + deltaX,xMin,xMax);
        var newY = Mathf.Clamp(transform.position.y + deltaY,yMin,yMax);
        transform.position = new Vector3(newX, newY);
    }
}
