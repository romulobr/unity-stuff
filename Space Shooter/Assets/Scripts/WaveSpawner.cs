﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    public WaveConfig[] waves;
    private Coroutine spawnRoutine;

    void Start() {
        StartCoroutine(SpawnEnemy(waves[0]));
        StartCoroutine(SpawnEnemy(waves[1]));
    }

    void Update() {

    }

    IEnumerator SpawnEnemy(WaveConfig wave)
    {
        Debug.Log("Spawning enemies for wave " + wave.name);
        var pathTransforms = new List<Transform>(wave.GetPaths());
        foreach (Transform t in pathTransforms){
            Debug.Log(t.position);
        }
        Debug.Log("Path transforms:"+pathTransforms.Count);
        for (int i = 0; i < wave.GetNumberOfEnemies(); i++)
        {            
            var enemy = Instantiate(wave.GetEnemyPrefab());
            enemy.GetComponent<EnemyPathing>().setWaypoints(pathTransforms);
            Debug.Log("Spawning enemy" + enemy.name+" (wave: "+wave.name+")");
            yield return new WaitForSeconds(wave.GetTimeBetweenSpawns());
        }
    }
}
