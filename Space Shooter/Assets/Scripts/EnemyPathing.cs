﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour {
    [SerializeField] List<Transform> waypoints;
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] int waypointIndex = 0;

    public EnemyPathing(List<Transform> waypoints, float moveSpeed)
    {
        this.waypoints = waypoints;
        this.moveSpeed = moveSpeed;
    }

    public void setWaypoints(List<Transform> waypoints)
    {
        this.waypoints = waypoints;
        transform.position = waypoints[0].position;
        waypointIndex = 1;
    }

    void Start () {
        if (waypoints!=null)
        {
            transform.position = waypoints[waypointIndex].position;
            waypointIndex = 1;
        }             
    }

	void Update ()
    {
        Move();
    }

    private void Move()
    {
        if(waypointIndex <= waypoints.Count - 1)
        {
            var targetPosition = waypoints[waypointIndex].transform.position;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
            if (transform.position.Equals(targetPosition))
            {
                waypointIndex++;
                if (waypointIndex >= waypoints.Count)
                {
                    waypointIndex = 0;
                }
            }
        }
    }
}
