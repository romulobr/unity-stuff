﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    public void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextScene = currentSceneIndex + 1;
        SceneManager.LoadScene(nextScene);
    }

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level1");
        FindObjectOfType<GameSession>().Die();
    }

    public void LoadGameOver()
    {        
        SceneManager.LoadScene(SceneManager.GetSceneByName("Game Over").buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
