﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedRegulator : MonoBehaviour {

    [SerializeField] float slowSpeed=1.0f;
    [SerializeField] float fastSpeed=5.0f;
    [SerializeField] Ball ball;
    float lowestY = 16;

    void Start () {
        var blocks = FindObjectsOfType<Block>();
        foreach (Block block in blocks)
        {
            var y = block.transform.position.y;
            if (y < lowestY)
            {
                lowestY = y;
            }
        }
    }
		
	void Update () {
        if (ball.transform.position.y <= lowestY)
        {
            Time.timeScale = slowSpeed;
        }
        else
        {
            Time.timeScale = fastSpeed;
        }
	}

}
