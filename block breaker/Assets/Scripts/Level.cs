﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {
    [SerializeField] int breakableBlocks;
    [SerializeField] SceneLoader sceneLoader;

    public void CountBreakableBlock() {
        breakableBlocks++;
    }

    public void RemovesBreakableBlock()
    {
        breakableBlocks--;
        if (breakableBlocks == 0)
        {
            sceneLoader.LoadNextLevel();
        }
    }    
}
