﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    [SerializeField] private float screenWidthInUnits = 16f;
    [SerializeField] private float minX = 1f;
    [SerializeField] private float maxX = 15f;
    [SerializeField] GameSession gameSession;
    [SerializeField] Ball ball;

    void Start () {
		
	}
	
	void Update () {
        float x = gameSession.isAutoPlayEnabled() ? ball.transform.position.x : Input.mousePosition.x/Screen.width*screenWidthInUnits;
        Vector2 newPosition = new Vector2(Mathf.Clamp(x, minX, maxX), transform.position.y);
        transform.position = newPosition;
    }
}
