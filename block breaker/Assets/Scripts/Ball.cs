﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    [SerializeField] private Paddle paddle;

    private bool launching = true;
    private Vector2 paddleToBallDistance;
    [SerializeField] Vector2 initialVelocity = new Vector2(2f, 15f);
    [SerializeField] AudioClip[] sounds;
    [SerializeField] float randomFactor = 2f;
    Rigidbody2D myRigidbody2D;

    private AudioSource audioSource;

    public void OnCollisionEnter2D(Collision2D collision)
    {        
        audioSource.PlayOneShot(sounds[Random.Range(0,2)]);
        myRigidbody2D.velocity = new Vector2(myRigidbody2D.velocity.x +Random.Range(0f, randomFactor), myRigidbody2D.velocity.y+Random.Range(0f, randomFactor));
    }

    void Start () {
        paddleToBallDistance = transform.position - paddle.transform.position;
        audioSource = GetComponent<AudioSource>();
        myRigidbody2D = GetComponent<Rigidbody2D>();
    }

	void Update () {
        if (launching)
        {
            LockToPaddle();
        }
        LaunchOnMouseClick();
    }

    private void LaunchOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0) && launching == true)
        {
            launching = false;
            var rigidBody2D = GetComponent<Rigidbody2D>();            
            rigidBody2D.velocity = initialVelocity;
        }
    }

    private void LockToPaddle()
    {
        transform.position = (Vector2)paddle.transform.position + paddleToBallDistance;
    }
}
