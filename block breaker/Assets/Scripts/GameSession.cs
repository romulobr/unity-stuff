﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSession : MonoBehaviour {
    [SerializeField] private int pointsPerHit = 7;
    [SerializeField] private int currentScore = 0;
    [SerializeField] TMPro.TextMeshProUGUI scoreText;
    [SerializeField] bool autoplay = false;

    private void Awake()
    {
        var gameStatuses = FindObjectsOfType<GameSession>();
        if (gameStatuses.Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {        
        scoreText.text = currentScore.ToString();
    }

    public void AddToScore()
    {
        currentScore += pointsPerHit;
        scoreText.text = currentScore.ToString();
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public bool isAutoPlayEnabled()
    {
        return autoplay;
    }
}
