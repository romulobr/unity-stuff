﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
    [SerializeField] AudioClip breakSound;
    [SerializeField] GameObject blockSparklesVFX;
    [SerializeField] int maxHits;
    [SerializeField] Sprite[] hitSprites;
    [SerializeField] private int timesHit;

    Level level;

    void Start()
    {
        GetBreakableBlocks();
    }

    private void GetBreakableBlocks()
    {
        level = FindObjectOfType<Level>();
        if (tag == "Breakable")
        {
            level.CountBreakableBlock();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (tag == "Breakable")
        {
            HandleHit();
        }
    }

    private void HandleHit()
    {
        timesHit++;
        if (timesHit<hitSprites.Length)
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[timesHit];
        }        
        Debug.Log(gameObject.name+" hits: " + timesHit+"/"+maxHits);
        if (timesHit >= maxHits)
        {
            Debug.Log("destroyng block " + gameObject.name);
            DestroyBlock();
        }
    }

    private void DestroyBlock()
    {
        FindObjectOfType<GameSession>().AddToScore();
        AudioSource.PlayClipAtPoint(breakSound, transform.position);
        level.RemovesBreakableBlock();
        TriggerSparklesVFX();
        Destroy(gameObject);
    }

    private void TriggerSparklesVFX()
    {
        if (tag == "Breakable")
        {
            var sparkles = Instantiate(blockSparklesVFX, transform.position, transform.rotation);
            Destroy(sparkles, 1);
        }
    }
}
