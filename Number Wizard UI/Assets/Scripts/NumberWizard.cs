﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour {

    [SerializeField] int min;
    [SerializeField] int max;
    int guess;
    [SerializeField] TextMeshProUGUI guessText;

    void nextGuess()
    {
        guess = Random.Range(min,max+1);
        guessText.text = guess.ToString();
    }

    public void onPressHigher()
    {
        min = guess+1;
        nextGuess();
    }

    public void onPressLower()
    {
        max = guess-1;
        nextGuess();
    }

    public void StartGame()
    {
        nextGuess();
    }

    void Start () {
        StartGame();
	}
	
	
	void Update () {
		
	}

}
