﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxController : MonoBehaviour {

    [SerializeField] float scrollSpeed=1.0f;
    [SerializeField] private Material material;
	
	void Update () {
        material.mainTextureOffset = new Vector2(material.mainTextureOffset.x+Time.deltaTime*scrollSpeed, material.mainTextureOffset.y);
	}
}
