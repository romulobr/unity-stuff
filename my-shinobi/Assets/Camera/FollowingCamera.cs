﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingCamera : MonoBehaviour {
    Camera theCamera;
    [SerializeField] GameObject player;
    [SerializeField] Vector3 distance;
    [SerializeField] float horizontalMaximumLimit;
    [SerializeField] float horizontalMinimumLimit;
    [SerializeField] float verticalMaximumLimit;
    [SerializeField] float verticalMinimumLimit;

    void Start () {
        theCamera = GetComponent<Camera>();
	}
		
	void Update () {
        distance = player.transform.position - transform.position;
        float newX = transform.position.x;
        float newY = transform.position.y;
        if (distance.x > 1.0f)
        {
            newX = player.transform.position.x - 1.0f;
        }
        else if (distance.x < -1.0f)
        {
            newX = player.transform.position.x + 1.0f;
        }
        if (distance.y > 0.5f)
        {
            newY = player.transform.position.y - 0.5f;
        }
        else if (distance.y < -0.5f)
        {
            newY = player.transform.position.y + 0.5f;
        }

        theCamera.transform.position = new Vector3(newX, newY, transform.position.z);
    }
}
