﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Weapon {

    [SerializeField] float attackDurationInseconds=0.3f;
    [SerializeField] float distanceFromOriginX = 0.7f;
    [SerializeField] float distanceFromOriginY = 0.1f;
    private SpriteRenderer spriteRenderer;

    public override void StartAttack(Vector2 position, bool facingLeft)
    {
        if (facingLeft)
        {
            position = new Vector2(position.x - distanceFromOriginX, position.y + distanceFromOriginY);
            spriteRenderer.flipX = true;
        }
        else
        {
            position = new Vector2(position.x + distanceFromOriginX, position.y + distanceFromOriginY);
            spriteRenderer.flipX = false;
        }
        spriteRenderer.transform.position = position;
        spriteRenderer.enabled = true;

    }

    public override void EndAttack()
    {
        spriteRenderer.enabled = false;
    }

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }    
}
