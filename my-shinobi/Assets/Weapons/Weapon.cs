﻿using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public abstract void StartAttack(Vector2 position, bool facingLeft);
    public abstract void EndAttack();
}