﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

    BoxCollider2D colider;

    public void Start()
    {
        colider = GetComponent<BoxCollider2D>();
    }

    public void OnExplosionBegins()
    {
        colider.size = new Vector2(0.5f,0.5f);
    }

    public void OnExplosionGrows()
    {
        colider.size = new Vector2(1.0f, 1.0f);
    }

    public void OnExplosionEnds()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {        
        //Debug.Log("Explosion hit something: "+ collision.gameObject.tag);
        if(collision.gameObject.tag == "Player"){
            NinjaController ninja = collision.GetComponent<NinjaController>();
            ninja.TakeDamage();
        }
    }
}
