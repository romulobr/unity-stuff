﻿using System;
using System.Collections;
using UnityEngine;

public class NinjaController : MonoBehaviour {
    [SerializeField] float moveSpeed = 30.0f;
    [SerializeField] float jumpSpeed = 10.0f;
    [SerializeField] GameObject swordGameObject;
    [SerializeField] float jumpSpeedAccumulatorLimit = 100.0f;
    
    Weapon sword;
    private Animator animator;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D body;

    [SerializeField] bool isFacingLeft;
    [SerializeField] int movementDirection;
    [SerializeField] float jumpSpeedAccumulator = 0.0f;

    [SerializeField] private float rayDistance = 1.0f;
    [SerializeField] private bool isOnTheGround;
    [SerializeField] private bool isMoving;
    [SerializeField] private bool isAttacking;
    [SerializeField] private bool isJumping;
    [SerializeField] private bool isFalling;
    [SerializeField] private bool isDucking;
    [SerializeField] private bool attacked = false;
    [SerializeField] private bool isTakingDamage;
    [SerializeField] private bool previousIsDead;
    [SerializeField] private bool isDead = false;
    [SerializeField] private bool isInvulnerable = false;

    public void Die()
    {
        isDead = true;
        animator.SetBool("isDead", isDead);
    }

    public void TakeDamage()
    {
        StartCoroutine(Damage());
    }

    void UpdateAnimatorAndSpriteRenderer()
    {                
        animator.SetBool("isMoving", isMoving);
        animator.SetBool("isDucking", isDucking);
        animator.SetBool("isAttacking", isAttacking);
        spriteRenderer.flipX = isFacingLeft;
        animator.SetBool("isJumping", isJumping);
        animator.SetBool("isFalling", isFalling);
        animator.SetBool("isTakingDamage", isTakingDamage);
    }

    void Start() {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        body = GetComponent<Rigidbody2D>();
        sword = (Weapon)GameObject.FindObjectOfType(typeof(Weapon));

        isMoving = false;
        isAttacking = false;
        isJumping=false;
        isFalling=false;

        UpdateAnimatorAndSpriteRenderer();        
    }

    float GetHorizontalMovement()
    {
        if (isAttacking && isOnTheGround || isDead || isTakingDamage)
        {
            isMoving = false;
            movementDirection = 0;
            return 0;
        }else 
        {
            var horizontal = Input.GetAxisRaw("Horizontal");
            if (horizontal < -0.1)
            {
                isFacingLeft = true;
                movementDirection = -1;
            }
            else if (horizontal > 0.1)
            {
                isFacingLeft = false;
                movementDirection = 1;
            }
            else
            {
                movementDirection = 0;
            }
            var speed = Time.fixedDeltaTime * moveSpeed * movementDirection;
            return speed;            
        }
    }

    bool isTouchingGround()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = rayDistance;

        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance,1<<0);
        if (hit.collider != null)
        {
            return true;
        }
        return false;
    }

    void HandleAttack()
    {
        if(Input.GetButtonDown("Attack")){
            StartCoroutine(Attack());            
        }
    }

    void HandleDuck() {
        if (Input.GetAxisRaw("Vertical")<0)
        {
            isDucking = true;
        }
        else
        {
            isDucking = false;
        }
    }

    void HandleJump()
    {
        float speed = Time.fixedDeltaTime * jumpSpeed;
        isOnTheGround = isTouchingGround();
        if (isOnTheGround && !isJumping)
        {
            jumpSpeedAccumulator = 0.0f;
            isJumping = false;
            isFalling = false;

            if (Input.GetButtonDown("Jump") && !isAttacking)
            {
                body.AddForce(new Vector2(0, jumpSpeed));
                isJumping = true;
                isFalling = false;
            }
        }
        else
        {
            if (isJumping)
            {
                if (Input.GetButtonUp("Jump") && !isAttacking)
                {
                    body.velocity = new Vector2(body.velocity.x, 0);
                    isJumping = true;
                    isFalling = false;
                }
                if (body.velocity.y > 0.0f)
                {
                    isJumping = true;
                    isFalling = false;
                } else
                {
                    isJumping = false;
                    isFalling = true;
                }
            }
        }
    }

    IEnumerator Damage(){
        if(!isInvulnerable && !isTakingDamage){
            isInvulnerable = true;
            isTakingDamage = true;
            if(isFacingLeft){
                body.AddForce(new Vector2(80.0f, 100.0f));
            }else{
                body.AddForce(new Vector2(-80.0f, 100.0f));
            }

            animator.SetBool("isTakingDamage", isTakingDamage);
            yield return new WaitForSeconds(0.5f);
            body.velocity = (new Vector2(0.0f,0.0f));
            isTakingDamage = false;
            animator.SetBool("isTakingDamage", isTakingDamage);
            yield return new WaitForSeconds(1.0f);
            isInvulnerable = false;
        }
    }

    IEnumerator Attack()
    {        
        if (!attacked)
        {
            isAttacking = true;
            if (isDucking)
            {
                sword.StartAttack(new Vector2(transform.position.x, transform.position.y - 0.3f), isFacingLeft);
                UpdateAnimatorAndSpriteRenderer();                
            }
            else
            {
                sword.StartAttack(transform.position, isFacingLeft);
                UpdateAnimatorAndSpriteRenderer();
            }
            attacked = true;
            yield return new WaitForSeconds(0.3f);
            sword.EndAttack();
            yield return new WaitForSeconds(0.1f);
            isAttacking = false;
            UpdateAnimatorAndSpriteRenderer();
            attacked = false;
        }
    }

    void onAttackEnded()
    {
        isAttacking = false;        
        UpdateAnimatorAndSpriteRenderer();
    }

    void Update () {
        HandleDuck();
        HandleAttack();        
        var horizontalMovement = GetHorizontalMovement();
        body.position = new Vector2(body.position.x + horizontalMovement, body.position.y);
        isMoving = horizontalMovement !=0 && !isFalling && !isJumping && !isAttacking && !isTakingDamage && !isDead;
        HandleJump();
        UpdateAnimatorAndSpriteRenderer();                
	}
}
